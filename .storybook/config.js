//-- core
import React from 'react';
import {configure, addDecorator, setAddon} from '@storybook/react';

import '../src/index.css';
import '../src/App.scss'

const req = require.context('../src', true, /.stories.js$/);
const loadStories = () => {
    req.keys().forEach(filename => {
        return req(filename)
    })
}
configure(loadStories, module);

//-- things
const PadMe = (storyFn) => {
    return (
        <div style={{padding: 20}}>
            {storyFn()}
        </div>
    )
};
addDecorator(PadMe);
