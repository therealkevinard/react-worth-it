export const basicState = {
    todos: [],
    ticks: 0,
    comparisons: [
        {
            id: 1,
            title: 'Coffee Pot',
            timeseries: [
                {
                    id: 1,
                    name: 'Coffee',
                    start: '2018-10-01',
                    stop: '2019-10-31',
                    cost_number: 9,
                    increment_unit: 1,
                    increment_type: 'weeks',
                    dates: [
                        {
                            date: '2018-10-01',
                            expense: 9
                        },
                        {
                            date: '2018-10-08',
                            expense: 18
                        },
                        {
                            date: '2018-10-15',
                            expense: 27
                        },
                        {
                            date: '2018-10-22',
                            expense: 36
                        },
                        {
                            date: '2018-10-29',
                            expense: 45
                        },
                        {
                            date: '2018-11-05',
                            expense: 54
                        },
                        {
                            date: '2018-11-12',
                            expense: 63
                        },
                        {
                            date: '2018-11-19',
                            expense: 72
                        },
                        {
                            date: '2018-11-26',
                            expense: 81
                        },
                        {
                            date: '2018-12-03',
                            expense: 90
                        },
                        {
                            date: '2018-12-10',
                            expense: 99
                        },
                        {
                            date: '2018-12-17',
                            expense: 108
                        },
                        {
                            date: '2018-12-24',
                            expense: 117
                        },
                        {
                            date: '2018-12-31',
                            expense: 126
                        },
                        {
                            date: '2019-1-07',
                            expense: 135
                        },
                        {
                            date: '2019-1-14',
                            expense: 144
                        },
                        {
                            date: '2019-1-21',
                            expense: 153
                        },
                        {
                            date: '2019-1-28',
                            expense: 162
                        },
                        {
                            date: '2019-2-04',
                            expense: 171
                        },
                        {
                            date: '2019-2-11',
                            expense: 180
                        },
                        {
                            date: '2019-2-18',
                            expense: 189
                        },
                        {
                            date: '2019-2-25',
                            expense: 198
                        },
                        {
                            date: '2019-3-04',
                            expense: 207
                        },
                        {
                            date: '2019-3-11',
                            expense: 216
                        },
                        {
                            date: '2019-3-18',
                            expense: 225
                        },
                        {
                            date: '2019-3-25',
                            expense: 234
                        },
                        {
                            date: '2019-4-01',
                            expense: 243
                        },
                        {
                            date: '2019-4-08',
                            expense: 252
                        },
                        {
                            date: '2019-4-15',
                            expense: 261
                        },
                        {
                            date: '2019-4-22',
                            expense: 270
                        },
                        {
                            date: '2019-4-29',
                            expense: 279
                        },
                        {
                            date: '2019-5-06',
                            expense: 288
                        },
                        {
                            date: '2019-5-13',
                            expense: 297
                        },
                        {
                            date: '2019-5-20',
                            expense: 306
                        },
                        {
                            date: '2019-5-27',
                            expense: 315
                        },
                        {
                            date: '2019-6-03',
                            expense: 324
                        },
                        {
                            date: '2019-6-10',
                            expense: 333
                        },
                        {
                            date: '2019-6-17',
                            expense: 342
                        },
                        {
                            date: '2019-6-24',
                            expense: 351
                        },
                        {
                            date: '2019-7-01',
                            expense: 360
                        },
                        {
                            date: '2019-7-08',
                            expense: 369
                        },
                        {
                            date: '2019-7-15',
                            expense: 378
                        },
                        {
                            date: '2019-7-22',
                            expense: 387
                        },
                        {
                            date: '2019-7-29',
                            expense: 396
                        },
                        {
                            date: '2019-8-05',
                            expense: 405
                        },
                        {
                            date: '2019-8-12',
                            expense: 414
                        },
                        {
                            date: '2019-8-19',
                            expense: 423
                        },
                        {
                            date: '2019-8-26',
                            expense: 432
                        },
                        {
                            date: '2019-9-02',
                            expense: 441
                        },
                        {
                            date: '2019-9-09',
                            expense: 450
                        },
                        {
                            date: '2019-9-16',
                            expense: 459
                        },
                        {
                            date: '2019-9-23',
                            expense: 468
                        },
                        {
                            date: '2019-9-30',
                            expense: 477
                        },
                        {
                            date: '2019-10-07',
                            expense: 486
                        },
                        {
                            date: '2019-10-14',
                            expense: 495
                        },
                        {
                            date: '2019-10-21',
                            expense: 504
                        },
                        {
                            date: '2019-10-28',
                            expense: 513
                        }
                    ]
                },
                {
                    id: 2,
                    name: 'Filters',
                    start: '2018-10-01',
                    stop: '2019-10-31',
                    cost_number: 5,
                    increment_unit: 1,
                    increment_type: 'months',
                    dates: [
                        {
                            date: '2018-10-01',
                            expense: 3
                        },
                        {
                            date: '2018-11-01',
                            expense: 6
                        },
                        {
                            date: '2018-12-01',
                            expense: 9
                        },
                        {
                            date: '2019-1-01',
                            expense: 12
                        },
                        {
                            date: '2019-2-01',
                            expense: 15
                        },
                        {
                            date: '2019-3-01',
                            expense: 18
                        },
                        {
                            date: '2019-4-01',
                            expense: 21
                        },
                        {
                            date: '2019-5-01',
                            expense: 24
                        },
                        {
                            date: '2019-6-01',
                            expense: 27
                        },
                        {
                            date: '2019-7-01',
                            expense: 30
                        },
                        {
                            date: '2019-8-01',
                            expense: 33
                        },
                        {
                            date: '2019-9-01',
                            expense: 36
                        },
                        {
                            date: '2019-10-01',
                            expense: 39
                        }
                    ]
                }
            ]
        },
        {
            id: 2,
            title: 'Gas Station Coffee',
            timeseries: [
                {
                    id: 1,
                    name: 'Cup of coffee',
                    start: '2018-10-01',
                    stop: '2019-10-31',
                    cost_number: 2.25,
                    increment_unit: 1,
                    increment_type: 'days',
                    dates: [
                        {
                            "date": "2018-10-01",
                            "cost": 2.25
                        },
                        {
                            "date": "2018-10-08",
                            "cost": 4.5
                        },
                        {
                            "date": "2018-10-15",
                            "cost": 6.75
                        },
                        {
                            "date": "2018-10-22",
                            "cost": 9
                        },
                        {
                            "date": "2018-10-29",
                            "cost": 11.25
                        },
                        {
                            "date": "2018-11-05",
                            "cost": 13.5
                        },
                        {
                            "date": "2018-11-12",
                            "cost": 15.75
                        },
                        {
                            "date": "2018-11-19",
                            "cost": 18
                        },
                        {
                            "date": "2018-11-26",
                            "cost": 20.25
                        },
                        {
                            "date": "2018-12-03",
                            "cost": 22.5
                        },
                        {
                            "date": "2018-12-10",
                            "cost": 24.75
                        },
                        {
                            "date": "2018-12-17",
                            "cost": 27
                        },
                        {
                            "date": "2018-12-24",
                            "cost": 29.25
                        },
                        {
                            "date": "2018-12-31",
                            "cost": 31.5
                        },
                        {
                            "date": "2019-1-07",
                            "cost": 33.75
                        },
                        {
                            "date": "2019-1-14",
                            "cost": 36
                        },
                        {
                            "date": "2019-1-21",
                            "cost": 38.25
                        },
                        {
                            "date": "2019-1-28",
                            "cost": 40.5
                        },
                        {
                            "date": "2019-2-04",
                            "cost": 42.75
                        },
                        {
                            "date": "2019-2-11",
                            "cost": 45
                        },
                        {
                            "date": "2019-2-18",
                            "cost": 47.25
                        },
                        {
                            "date": "2019-2-25",
                            "cost": 49.5
                        },
                        {
                            "date": "2019-3-04",
                            "cost": 51.75
                        },
                        {
                            "date": "2019-3-11",
                            "cost": 54
                        },
                        {
                            "date": "2019-3-18",
                            "cost": 56.25
                        },
                        {
                            "date": "2019-3-25",
                            "cost": 58.5
                        },
                        {
                            "date": "2019-4-01",
                            "cost": 60.75
                        },
                        {
                            "date": "2019-4-08",
                            "cost": 63
                        },
                        {
                            "date": "2019-4-15",
                            "cost": 65.25
                        },
                        {
                            "date": "2019-4-22",
                            "cost": 67.5
                        },
                        {
                            "date": "2019-4-29",
                            "cost": 69.75
                        },
                        {
                            "date": "2019-5-06",
                            "cost": 72
                        },
                        {
                            "date": "2019-5-13",
                            "cost": 74.25
                        },
                        {
                            "date": "2019-5-20",
                            "cost": 76.5
                        },
                        {
                            "date": "2019-5-27",
                            "cost": 78.75
                        },
                        {
                            "date": "2019-6-03",
                            "cost": 81
                        },
                        {
                            "date": "2019-6-10",
                            "cost": 83.25
                        },
                        {
                            "date": "2019-6-17",
                            "cost": 85.5
                        },
                        {
                            "date": "2019-6-24",
                            "cost": 87.75
                        },
                        {
                            "date": "2019-7-01",
                            "cost": 90
                        },
                        {
                            "date": "2019-7-08",
                            "cost": 92.25
                        },
                        {
                            "date": "2019-7-15",
                            "cost": 94.5
                        },
                        {
                            "date": "2019-7-22",
                            "cost": 96.75
                        },
                        {
                            "date": "2019-7-29",
                            "cost": 99
                        },
                        {
                            "date": "2019-8-05",
                            "cost": 101.25
                        },
                        {
                            "date": "2019-8-12",
                            "cost": 103.5
                        },
                        {
                            "date": "2019-8-19",
                            "cost": 105.75
                        },
                        {
                            "date": "2019-8-26",
                            "cost": 108
                        },
                        {
                            "date": "2019-9-02",
                            "cost": 110.25
                        },
                        {
                            "date": "2019-9-09",
                            "cost": 112.5
                        },
                        {
                            "date": "2019-9-16",
                            "cost": 114.75
                        },
                        {
                            "date": "2019-9-23",
                            "cost": 117
                        },
                        {
                            "date": "2019-9-30",
                            "cost": 119.25
                        }
                    ]
                }
            ]
        },
    ]
};

export const sampleTimeseries = [
    {
        name: 'coffee',
        start: '2018-10-01',
        stop: '2019-10-01',
        cost_number: 9,
        increment_unit: 1,
        increment_type: 'weeks',
    }
]
