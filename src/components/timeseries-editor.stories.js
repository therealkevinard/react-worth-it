import React from 'react';
import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {basicState, sampleTimeseries} from '../../.storybook/harness';

import {emptyTimeseries, TimeseriesEditor} from './timeseries-editor';

export const actions = {
    onUpdateTimeline: action('onUpdateTimeline'),
    onRemoveTimeline: action('onRemoveTimeline'),
};

storiesOf('Timeseries Editor', module)
    .addDecorator(item => {
        return (
            <div style={{maxWidth: 200}}>{item()}</div>
        )
    })
    .add('default', () => {
        return (
            <TimeseriesEditor timeseries={emptyTimeseries} {...actions} />
        )
    })
    .add('filled', () => {
        return (
            <TimeseriesEditor timeseries={sampleTimeseries[0]} {...actions}/>
        )
    })