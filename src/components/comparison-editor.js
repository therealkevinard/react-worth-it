import React from 'react';
import {connect} from 'react-redux';
import {StyleSheet, css} from 'aphrodite';
import {emptyTimeseries, TimeseriesEditor} from "./timeseries-editor";

const styles = StyleSheet.create({});

export class ComparisonEditor extends React.Component {
    /**
     * @param timeseries
     */
    constructor({comparison}) {
        super();
        this.state = comparison;
        //--
        this._onAddNewTimeseries = this._onAddNewTimeseries.bind(this);
        this._onUpdateTimeseries = this._onUpdateTimeseries.bind(this);
        this._onRemoveTimeseries = this._onRemoveTimeseries.bind(this);
    }

    _onAddNewTimeseries() {
        let t = this.state.timeseries;
        t.push(emptyTimeseries);
        this.setState({timeseries: t})
    }

    _onUpdateTimeseries(payload) {
        let wasUpdated = false;
        let updated = this.state.timeseries.map(ts => {
            if (wasUpdated) {
                return ts;
            }
            if (ts.id === payload.id) {
                wasUpdated = true;
                return payload;
            } else {
                return ts;
            }
        })
        this.setState({timeseries: updated}); // @todo: push new data to store here?
    }

    _onRemoveTimeseries(payloadId) {
        let updated = this.state.timeseries.filter(ts => {
            return ts.id !== payloadId;
        })
        this.setState({timeseries: updated}) // @todo: push to store
    }

    render() {
        return (
            <div>
                <div>Editing {this.state.title}</div>

                <div>
                    <button onClick={this._onAddNewTimeseries}>Add New</button>
                </div>

                {this.state.timeseries.length && this.state.timeseries.map((ts, i) => {
                    return (
                        <TimeseriesEditor key={i}
                                          timeseries={ts}
                                          onUpdateTimeline={this._onUpdateTimeseries}
                                          onRemoveTimeline={this._onRemoveTimeseries}
                        />
                    )
                })}
            </div>
        )
    }
}

export default connect(
    state => {
        return {}
    },
    dispatch => {
        return {}
    }
)(ComparisonEditor)