import React from 'react';
import {StyleSheet, css} from 'aphrodite';
import {connect} from 'react-redux';
import {ComparisonItem} from "./comparison-item";

const styles = StyleSheet.create({
    comparisonList: {},
    comparisonListItem: {
        marginTop: 8
    },
})

export const ComparisonList = ({items}) => {
    return (
        <div className={css(styles.comparisonList)}>
            {items.map(item => {
                return (
                    <div className={css(styles.comparisonListItem)}>
                        <ComparisonItem key={item.index} item={item}/>
                    </div>
                )
            })}
        </div>
    )
};

export default connect(
    state => {
        return {}
    },
    dispatch => {
        return {}
    }
)(ComparisonList)