import React from 'react';
import {storiesOf} from '@storybook/react';
import {basicState} from '../../.storybook/harness';

import {ChartTimeline} from "./chart-timeline";

storiesOf('Timeline Chart', module)
    .add('default', () => {
        return (
            <ChartTimeline data={basicState.comparisons} width={800} height={600}/>
        )
    })