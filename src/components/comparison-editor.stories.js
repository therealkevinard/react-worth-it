import React from 'react';
import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';

import {basicState, sampleTimeseries} from '../../.storybook/harness';

import {ComparisonEditor} from "./comparison-editor";

export const actions = {
    onUpdateTimeline: action('onUpdateTimeline')
};

storiesOf('Comparison Editor', module)
    .addDecorator(item => {
        return (
            <div style={{maxWidth: 200}}>{item()}</div>
        )
    })
    .add('compare-0', () => {
        return (
            <ComparisonEditor {...actions} comparison={basicState.comparisons[0]}/>
        )
    })
    .add('compare-1', () => {
        return (
            <ComparisonEditor {...actions} comparison={basicState.comparisons[1]}/>
        )
    })