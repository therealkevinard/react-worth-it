import React from 'react';
import {storiesOf} from '@storybook/react';
import {basicState} from '../../.storybook/harness';

import {ComparisonItem} from './comparison-item';

storiesOf('Comparison Item', module)
    .add('default', () => {
        return (
            <ComparisonItem item={basicState.comparisons[0]}/>
        )
    })