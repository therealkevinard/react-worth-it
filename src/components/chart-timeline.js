import React from 'react';
import {connect} from 'react-redux';
import {StyleSheet, css} from 'aphrodite';
import {curveCatmullRom} from 'd3-shape';
import {
    XYPlot,
    XAxis,
    YAxis,
    ChartLabel,
    HorizontalGridLines,
    VerticalGridLines,
    LineSeries,
    LineMarkSeries
} from 'react-vis';

const styleDefs = {
    grid: {
        stroke: 'blue',
        strokeWidth: 0.25,
        strokeDasharray: '1,5'
    },
    line: {
        line: {
            strokeDasharray: '2,2'
        },
        mark: {
            strokeWidth: 0.1,
            r: 2,
            opacity: 0.25
        },
    }
};

const styles = StyleSheet.create(styleDefs)
const colorScales = [
    'blue', 'green', 'orange', 'tomato'
];

/**
 *
 * @param data
 * @param width
 * @param height
 * @return {*}
 * @constructor
 */
export const ChartTimeline = ({data, width, height}) => {
    return (
        <div>
            <XYPlot
                xType="time"
                width={width}
                height={height}>
                <HorizontalGridLines style={styleDefs.grid}/>
                <VerticalGridLines style={styleDefs.grid}/>
                <XAxis/>
                <YAxis tickFormat={v => `$${v}`} left={10}/>

                {data.map((comparison, i_comparison) => {
                    return comparison.timeseries.map((timeseries, i_timeseries) => {
                        console.log(`ts; ${i_comparison}:${i_timeseries}`)
                        let d = timeseries.dates.map(date => {
                            return {x: new Date(date.date), y: date.expense}
                        });
                        let color = colorScales[Math.floor(Math.random() * colorScales.length)];
                        return (
                            <LineMarkSeries
                                className="first-series"
                                lineStyle={Object.assign({}, styleDefs.line.line, {stroke: color})}
                                markStyle={Object.assign({}, styleDefs.line.mark, {stroke: color, fill: color})}
                                data={d}
                            />
                        )
                    })
                })}
            </XYPlot>
        </div>
    )
};

export default connect(
    state => {
        return {}
    },
    dispatch => {
        return {}
    }
)(ChartTimeline)