import React from 'react';
import {connect} from 'react-redux';
import {StyleSheet, css} from 'aphrodite';
import {calculateTimeline} from '../util/date'
import './timeseries-editor.scss';

const styles = StyleSheet.create({});

export const emptyTimeseries = {
    name: '',
    start: '',
    stop: '',
    cost_number: 0,
    increment_unit: 0,
    increment_type: '',

};

export class TimeseriesEditor extends React.Component {
    /**
     * @param timeseries
     */
    constructor({timeseries, onUpdateTimeline, onRemoveTimeline}) {
        super();
        //--
        this.state = Object.assign({}, emptyTimeseries, timeseries);
        //--
        this._onInputChange = this._onInputChange.bind(this);
        this._onSubmit = this._onSubmit.bind(this)
        this.onUpdateTimeline = onUpdateTimeline;
        this.onRemoveTimeline = onRemoveTimeline;
    }

    _onInputChange(key, value) {
        this.setState({[key]: value});
    }

    _onSubmit() {
        let timeline = calculateTimeline(
            this.state.start,
            this.state.stop,
            [
                this.state.increment_unit,
                this.state.increment_type
            ],
            this.state.cost_number
        );
        this.onUpdateTimeline(this.state, timeline);
    }

    render() {
        return (
            <div className={['timeseries-editor--outer']}>
                <h5>Timeseries <button onClick={() => {
                    this.onRemoveTimeline(this.state.id)
                }}>x</button></h5>
                <div>
                    <label htmlFor="">Expense Name</label>
                    <input type="text" name="name"
                           value={this.state.name}
                           onChange={e => {
                               this._onInputChange(e.currentTarget.name, e.currentTarget.value)
                           }}/>
                </div>
                <div>
                    <label htmlFor="">Start</label>
                    <input type="date" name="start"
                           value={this.state.start}
                           onChange={e => {
                               this._onInputChange(e.currentTarget.name, e.currentTarget.value)
                           }}/>
                </div>
                <div>
                    <label htmlFor="">End</label>
                    <input type="date" name="stop"
                           value={this.state.stop}
                           onChange={e => {
                               this._onInputChange(e.currentTarget.name, e.currentTarget.value)
                           }}/>
                </div>
                <hr/>

                <h6>Expense</h6>
                <div>
                    <label htmlFor="">cost</label>
                    <input type="number" min={1} name="cost_number"
                           value={this.state.cost_number}
                           onChange={e => {
                               this._onInputChange(e.currentTarget.name, e.currentTarget.value)
                           }}/>
                </div>
                <div>
                    <label htmlFor="">increment unit</label>
                    <input type="number" name="increment_unit"
                           value={this.state.increment_unit}
                           onChange={e => {
                               this._onInputChange(e.currentTarget.name, e.currentTarget.value)
                           }}/>
                </div>
                <div>
                    <label htmlFor="">increment type</label>
                    <input type="text" name="increment_type"
                           value={this.state.increment_type}
                           onChange={e => {
                               this._onInputChange(e.currentTarget.name, e.currentTarget.value)
                           }}/>
                </div>
                <hr/>

                <input type="button" value="Commit" onClick={this._onSubmit}/>
            </div>
        )
    }
}

export default connect(
    state => {
        return {}
    },
    dispatch => {
        return {}
    }
)(TimeseriesEditor)