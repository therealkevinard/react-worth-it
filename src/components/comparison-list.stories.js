import React from 'react';
import {storiesOf} from '@storybook/react';
import {basicState} from '../../.storybook/harness';

import {ComparisonList} from './comparison-list';

storiesOf('Comparison List', module)
    .add('default', () => {
        return (
            <ComparisonList items={basicState.comparisons}/>
        )
    })