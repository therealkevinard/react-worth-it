import React from 'react';
import {connect} from 'react-redux';

export const ComparisonItem = ({item}) => {
    return (
        <div>
            {item.id}: {item.title}
        </div>
    )
};

export default connect(
    (state) => {
        return {}
    },
    (dispatch) => {
        return {}
    }
)(ComparisonItem)