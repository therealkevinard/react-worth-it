import moment from 'moment';

const dateFormat = "Y-M-DD";
/**
 * Provides an array of dates (Y-M-DD) from {start} to {stop}, with {increment} steps
 * @param start
 *    first date in series
 * @param stop
 *    final date in series
 * @param increment
 *    momentjs-friendly increment parameters. eg: [2, 'days']
 * @returns {Array}
 */
export const getSeries = (start, stop, increment) => {
    if (increment[0] <= 0) {
        return false;
    }
    start = moment(start);
    stop = moment(stop);
    let dates = [];
    let curr = start;
    while (curr.isBefore(stop)) {
        dates.push(curr.format(dateFormat));
        curr.add(increment[0], increment[1]);
    }

    return dates;
}

export const sumOverTime = (dates, incrementCost) => {
    let cost = 0;
    incrementCost = parseFloat(incrementCost);

    return dates.map(date => {
        cost += incrementCost;
        return {
            date,
            cost
        }
    });
};

export const calculateTimeline = (start, stop, increment, incrementCost) => {
    let d = getSeries(start, stop, increment);
    return sumOverTime(d, incrementCost);
}